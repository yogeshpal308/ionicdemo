import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.positronx.pushnotification',
  appName: 'ionic-firebase-push-notification',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
